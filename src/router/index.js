import Vue from 'vue';
import Router from 'vue-router';
import AllTeams from '@/components/AllTeams';
import Team from '@/components/Team';
import NotFound from '@/components/NotFound';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: AllTeams
    },
    {
      path: '/team/:id',
      name: 'team',
      component: Team
    },
    {
      path: '*',
      name: '404',
      component: NotFound
    }
    // put your vue routes here
  ]
});
