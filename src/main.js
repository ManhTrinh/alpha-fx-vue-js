// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store/index';

import VueCurrencyFilter from 'vue-currency-filter';
import VeeValidate from 'vee-validate';

import './styles/main.scss';

// *** Set up API
import axios from 'axios';
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.baseURL = (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'development') ? process.env.ROOT_API : 'https://alphafx-code-test-api.herokuapp.com/api/';
Vue.prototype.$apiCallService = axios;
// Set up API ***

// Set up pipes DateFormat(momentjs) + CurrencyFormat(VueCurrencyFilter);
window.moment = require('moment');
Vue.use(require('vue-moment'));
Vue.use(VeeValidate);
Vue.use(VueCurrencyFilter, {
  symbol: '',
  thousandsSeparator: ',',
  fractionCount: 2,
  fractionSeparator: '.',
  symbolPosition: 'front',
  symbolSpacing: true
});

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
});
