import Vue from 'vue';
import Vuex from 'vuex';

import axios from 'axios';

Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    teams: [],
    test: 'test'
  },
  mutations: {
    setTeams (state, teams) {
      state.teams = teams;
    }
  },
  actions: {
    fetchTeams (context) {
      axios.post('teams').then(response => {
        context.commit('setTeams', response.data.data);
      });
    }
  }
});
